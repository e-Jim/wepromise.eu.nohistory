# Wepromise.eu

This is the software used for the wepromise.eu campain.
It can be used to run any campain asking for candidates to European Election to make some promise, but will need some adapations. (see the home/list_results/text_page views, the edri model,...)

It can handle signature of the promise by e-mail confirmation, sharing a supporter's kit, list PRomises etc...

The software has been developed using CodeIgniter (hence the licence) with the help of GoVeto (goveto.org).

To install it, there are a few steps to follow.

# Requirements

1. An Apache server (minor changes needs to be done to host it with Nginx: see the .htaccess file)
1. A Mysql server (>5.1) with a account name wepromise, which has access to a database name wepromise
1. Php >= 5.6

# Installation steps

1. Copy all the files here to you web root, be it through sftp or just by cloning this repository (git clone https://framagit.org/e-Jim/wepromise.eu.git as for now) 
1. Import the MySQL dump into your database: 
`mysql -uwepromise -p wepromise < database-export-no-personnal-data.sql` and type your password when prompted for it.
1. Modify the application/config/config.php file and modify the values of $config['encryption_key'] to anything secret and random enough. (I would generate it through a random generator), and the value of $config['base_url'] to your base URL (https://yourdomainname.tld). 
1. Modify the application/config/database.php file, and set the values of $db['default']['password'] to your actual MySQL password.
1. Modify admin's e-mail adress directly in MySQL:
`mysql -uwepromise -p wepromise -e "UPDATE users SET email='<INSERT-YOUR-EMAIL-HERE>' WHERE 'username'='admin'"` and type your password when prompted for it.
1. You might need to adapt the e-mail sending preferences in system/libraries/Email.php (more info on how to use it on https://codeigniter.com/user_guide/libraries/email.html)
1. Go the https://yourdomainname.tld/admin
1. Login with account 'admin' and password 'ModifyThisASAPMan-' (or you can modify it by clicking on Forgot Password, which will send you an password reset e-mail - or via domaine.tld/auth/change_password/ once you've already logged in).
1. You now have access to the admin section, where you can add/delete candidates countries etc... There are still data from EDRi's campain, as an example.
