<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/responsive_europe_map.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/frontend/js/carouFredSel-6.2.1/jquery.carouFredSel-6.2.1.js"></script>


<script type="text/javascript">
	var _countries_candidates = {
		<?php
			$index = 0;
			foreach($country_candidates as $country){
				$index ++;
				echo "'".$country->code."' : {id:".$country->id." ,name:' ".$country->name."', nr: ".$country->nr_candidates."}" . ($index < count($country_candidates)? ",":"")."\n" ; 
			}
		?>
	};
 
	$(function(){
	

	 

		 //map
		$("#rc_list").carouFredSel({
			circular: true,
			infinite: true,
			width:'100%',
			padding:'0',
			auto:false,
			items : 5,
			direction : "down",
			next : {
				button : "#rc_next_button",
				key : "up"
			}, scroll : {
				items : 2
			} 
		});
					
	
	
	
	
	
	
	
		//select

	
	
	
		//resize the map
		
		function select_country(_code){

		
			var tooltip_left = _tooltip_position[_code][0];
			var tooltip_top  =  _tooltip_position[_code][1];
			
			var tooltip = $('.map_tooltip');
			 
			tooltip.children('.label').html( _countries_candidates[_code].nr + " <?=$candidates?>");
			tooltip.children('.country').html( _countries_candidates[_code].name);
			
			var tooltip_width = tooltip.width() + 20;
			
			tooltip.css('margin-left', (-1 * tooltip_width/2)+ "px");
			tooltip.css('left', tooltip_left + "%");
			tooltip.css('top',tooltip_top + "%");
			tooltip.css('visibility','visible');
			
			var window_height = $(window).height();	
		}
		
		
		
		window.resize_imagemap();
			
		$(window).resize(function(){
			//resize the map each time the viewport is resized
			window.resize_imagemap();
		});
		
		$('map').children().mouseenter(function(){
			var _code = this.href.indexOf("#") + 1;
			_code = this.href.substring(_code);
			select_country(_code);
			document.getElementById('map').src = _countries[_code].src;
			$('.map_tooltip .country').width('auto');
			$('.candidates_wrapper').children().children().removeClass('on');
			$('.candidates_wrapper').children().children('[data-code="'+_code+'"]').addClass('on');
		});
		
		$('map').children().click(function(event){
			var _code = this.href.indexOf("#") + 1; 
			_code = this.href.substring(_code);
			select_country(_code);
			event.preventDefault();
			
			window.location = "<?=base_url()?><?=$this->uri->segment(1)?>/country/" + _countries_candidates[_code].id;

			
		});
		
		$('#map_container').children().mouseleave(function(){
			//document.getElementById('map').src = "<?=base_url()?>assets/frontend/img/map_v2.png";
		});
		
		$('.candidates_wrapper').children().children().click(function(){
			$('.candidates_wrapper').children().children().removeClass('on');
			$(this).addClass('on');
			_code = $(this).attr('data-code');
			select_country(_code);
			document.getElementById('map').src = _countries[_code].src;
			
			window.location = "<?=base_url()?><?=$this->uri->segment(1)?>/country/" + _countries_candidates[_code].id;

		});
		
		
	

		//tooltip
					

	});


</script>