<?=$header?> 
	<div id="page-wrapper">
		<?php if(validation_errors()):?>
			<div class="alert alert-dismissable alert-danger">
				<button type="button" class="close" data-dismiss="alert">�</button>
				<?=validation_errors()?>
				
			</div> 
		<?php endif;?>

		<?php if($this->session->flashdata('success')):?>
			<div class="alert alert-dismissable alert-success">
				<button type="button" class="close" data-dismiss="alert">�</button>
				<?=$this->session->flashdata('success')?>
			</div>
		<?php endif;?>		
		
		<div class="row">
			<div class="col-sm-12">
				<h1>Edit ZIP code > <?=$zip->zip?></h1>
				<ol class="breadcrumb">
					<li><a href="<?=base_url()?>admin"><i class="icon-dashboard"></i> Dashboard</a></li>
					<li><a href="<?=base_url()?>admin/countries">Countries</a></li>
					<li><a href="<?=base_url()?>admin/edit_city/<?=$city->id?>/<?=$city->country_id?>"><?=$city->name?></a></li>
					<li class="active"><i class="icon-file-alt"></i> ZIP code > <?=$zip->zip?></li>
				</ol>
			</div>
		</div><!-- /.row -->
		<div class="row">
			<div class="col-sm-12">
				<form method="post" action="">
					
					<div class="row" id="row1">
						<div class="col-sm-3">					
							<label class="control-label" for="zipcode">Zipcode</label>		
							<input value="<?=(set_value('zipcode'))?set_value('zipcode'):$zip->zip?>" autocomplete="off" class="form-control" id="zipcode" name="zipcode" type="text" />
						</div>		
						<div class="col-sm-1">					
							<label class="control-label" for="zipcode">&nbsp;</label>		<br/>
							<input class="btn btn-primary" type="submit" name="send" value="Save district">
						</div>		
						<div class="col-sm-1">					
							<label class="control-label" for="zipcode">&nbsp;</label>		<br/>
							<a class="btn btn-danger" href="<?=base_url()?>admin/delete_zip/<?=$zip->id?>/<?=$city->id?>/<?=$city->country_id?>">Delete</a>
						</div>									
					</div>	 

					<div class="row">
						<div class="col-sm-12">
						
							
						</div>
					</div>				
				</form>
			</div>  
		</div> 
	</div><!-- /#page-wrapper -->
<?=$footer?>