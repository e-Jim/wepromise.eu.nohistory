<?=$header?>
	<div id="page-wrapper">
		<?php if(validation_errors()):?>
			<div class="alert alert-dismissable alert-danger">
				<button type="button" class="close" data-dismiss="alert">�</button>
				<?=validation_errors()?>
				
			</div>
		<?php endif;?>
		<?php if($this->session->flashdata('success')):?>
			<div class="alert alert-dismissable alert-success">
				<button type="button" class="close" data-dismiss="alert">�</button>
				<?=$this->session->flashdata('success')?>
			</div>
		<?php endif;?>
		<div class="row">
			<div class="col-sm-12">
				<h1>Edit Page</h1>
				<ol class="breadcrumb">
				<li><a href="<?=base_url()?>admin"><i class="icon-dashboard"></i> Dashboard</a></li>
				<li class="active"><i class="icon-file-alt"></i>Edit page</li>
				</ol>
			</div>
		</div><!-- /.row -->
		<div class="row">
			<div class="col-sm-12">
				<form method="post" action="" >
					<div class="row">
						<div class="col-sm-4">
							<?php $name_val = (set_value('general_name'))?set_value('general_name'):$page['general_name']; ?>
							<?=form_label('Name','','class="form-control"')?><br/>
							<?=form_input('general_name',$name_val,'class="form-control"')?>
						</div>
					</div>
					<br/>
					<div class="row">
						<div class="col-sm-12">
							<label>Anchor</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>anchor">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li>
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>anchor">
									<br/>
									<?php 
										$translation = $this->edri->search_array($page['translations'],'language_id',$language->id);
										$translation = (isset($translation[0]['anchor']))?$translation[0]['anchor']:"";
										$anchor = (set_value('anchor'.$language->id))?set_value('anchor'.$language->id):$translation; 
										
										  
									?> 
									
										
								
									<input type="text" class="form-control" cols="4" name="anchor<?=$language->id?>" value="<?=$anchor?>">
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
					 
					</div> 	
					<br/>					
					<div class="row">
						<div class="col-sm-12">
							<label>Slug</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>slug">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li>
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>slug">
									<br/>
									<?php 
										$translation = $this->edri->search_array($page['translations'],'language_id',$language->id);
										$translation = (isset($translation[0]['slug']))?$translation[0]['slug']:"";
										$slug = (set_value('slug'.$language->id))?set_value('slug'.$language->id):$translation; 
										
										 
									?> 
									
										
								
									<input type="text" class="form-control" cols="4" name="slug<?=$language->id?>" value="<?=$slug?>">
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
					 
					</div> 	
					<br/>
					<div class="row">
						<div class="col-sm-12">
							<label>Content left</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>content">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li>
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>content">
									<br/>
									<?php 
										$translation = $this->edri->search_array($page['translations'],'language_id',$language->id);
										$translation = (isset($translation[0]['content']))?$translation[0]['content']:"";
										$content = (set_value('content'.$language->id))?set_value('content'.$language->id):$translation; 
										
										 
									?> 
									
										
								
									<textarea class="form-control" cols="4" style="height:300px" name="content<?=$language->id?>"><?=$content?></textarea>
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
					 
					</div> 
				<br/>
					<div class="row">
						<div class="col-sm-12">
							<label>Content center</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>content_2">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li>
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>content_2">
									<br/>
									<?php 
										$translation = $this->edri->search_array($page['translations'],'language_id',$language->id);
										$translation = (isset($translation[0]['content_2']))?$translation[0]['content_2']:"";
										$content_2 = (set_value('content_2'.$language->id))?set_value('content_2'.$language->id):$translation; 
										
										 
									?> 
									
										
								
									<textarea class="form-control" cols="4" style="height:300px" name="content_2<?=$language->id?>"><?=$content_2?></textarea>
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
					 
					</div> 
					<br/>
					<div class="row">
						<div class="col-sm-12">
							<label>Content right</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>content_3">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li>
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>content_3">
									<br/>
									<?php 
										$translation = $this->edri->search_array($page['translations'],'language_id',$language->id);
										$translation = (isset($translation[0]['content_3']))?$translation[0]['content_3']:"";
										$content_3 = (set_value('content_3'.$language->id))?set_value('content_3'.$language->id):$translation; 
										
										 
									?> 
									
										
								
									<textarea class="form-control" cols="4" style="height:300px" name="content_3<?=$language->id?>"><?=$content_3?></textarea>
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
					 
					</div> 					
					<br/>
					<div class="row">
						<div class="col-sm-12">
							<label>Meta title</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>meta_title">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li>
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>meta_title">
									<br/>
									<?php 
										$translation = $this->edri->search_array($page['translations'],'language_id',$language->id);
										$translation = (isset($translation[0]['meta_title']))?$translation[0]['meta_title']:"";
										$meta_title = (set_value('meta_title'.$language->id))?set_value('meta_title'.$language->id):$translation; 
										
										 
									?> 
									
										
								
									<input type="text" class="form-control" cols="4" name="meta_title<?=$language->id?>" value="<?=$meta_title?>">
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
					 
					</div> 						
					<br/>
					<div class="row">
						<div class="col-sm-12">
							<label>Meta description</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>meta_description">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li>
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>meta_description">
									<br/>
									<?php 
										$translation = $this->edri->search_array($page['translations'],'language_id',$language->id);
										$translation = (isset($translation[0]['meta_description']))?$translation[0]['meta_description']:"";
										$meta_description = (set_value('meta_description'.$language->id))?set_value('meta_description'.$language->id):$translation; 
										
										 
									?> 
									
										
								
									<input type="text" class="form-control" cols="4" name="meta_description<?=$language->id?>" value="<?=$meta_description?>">
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
					 
					</div> 	
					<br/>
					<div class="row">
						<div class="col-sm-12">
							<label>Meta keywords</label>
						</div>
						<div class="col-sm-12 page_title">
							<ul class="nav nav-tabs">
								<?php $i=0;  foreach($languages as $language):?>
									<li class='test<?=($i==0)? ' active':''?>' data-toggle="tooltip" data-placement="top" title="" data-original-title="<?=$language->name?>"  >
										<a data-toggle="tab" href="#<?=strtolower($language->name)?>meta_keywords">
											<img width="25px"src="<?=base_url()?>assets/admin/img/flags/<?=$language->flag?>">
										</a>
									</li>
								<?php $i++; endforeach;?>					
							</ul> 
							<div class="tab-content">
							
							<?php $i=0; foreach($languages as $language):?>
								
								<div class="tab-pane<?=($i==0)? ' active':''?>" id="<?=strtolower($language->name)?>meta_keywords">
									<br/>
									<?php 
										$translation = $this->edri->search_array($page['translations'],'language_id',$language->id);
										$translation = (isset($translation[0]['meta_keywords']))?$translation[0]['meta_keywords']:"";
										$meta_keywords = (set_value('meta_keywords'.$language->id))?set_value('meta_keywords'.$language->id):$translation;  
									?> 
									
								 		
								
									<input type="text" class="form-control" cols="4" name="meta_keywords<?=$language->id?>" value="<?=$meta_keywords?>">
								</div>
							<?php $i++; endforeach;?>
							</div>
						</div>
					 
					</div> 	
					<br/>
					<div class="row">
						<div class="col-sm-12">
							<input class="btn btn-primary" type="submit" name="send" value="Update page">
						</div>
					</div>
				</form>
					 

		 
			</div>  
		</div>
	</div><!-- /#page-wrapper -->
<?=$footer?>