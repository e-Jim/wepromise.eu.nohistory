<script src="<?=base_url()?>assets/admin/js/tablesorter/jquery.tablesorter.js"></script>
<script src="<?=base_url()?>assets/admin/js/tablesorter/tablesorter_filter.js"></script>
<script src="<?=base_url()?>assets/admin/js/tablesorter/jquery.tablesorter.pager.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("table")
	.tablesorter({debug: false, widgets: ['zebra'], sortList: [[0,0]]})
	.tablesorterFilter({filterContainer: "#filter-box",
						filterClearContainer: "#filter-clear-button",
						filterColumns: [0]})
	.tablesorterPager({container: $("#pager")});
});

</script>