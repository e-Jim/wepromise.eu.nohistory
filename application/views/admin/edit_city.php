<?=$header?> 
	<div id="page-wrapper">
		<?php if(validation_errors()):?>
			<div class="alert alert-dismissable alert-danger">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<?=validation_errors()?>
				
			</div> 
		<?php endif;?>
		<?php if($this->session->flashdata('errors')):?>
			<div class="alert alert-dismissable alert-danger">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<?=$this->session->flashdata('errors')?>
			</div>
		<?php endif;?>	
		<?php if($this->session->flashdata('success')):?>
			<div class="alert alert-dismissable alert-success">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<?=$this->session->flashdata('success')?>
			</div>
		<?php endif;?>		
		
		<div class="row">
			<div class="col-sm-12">
				<h1>Edit city > <?=$city->name?></h1>
				<ol class="breadcrumb">
				<li><a href="<?=base_url()?>admin"><i class="icon-dashboard"></i> Dashboard</a></li>
				<li><a href="<?=base_url()?>admin/countries">Countries</a></li>
				<li class="active"><i class="icon-file-alt"></i> Edit city > <?=$city->name?></li>
				</ol>
			</div>
		</div><!-- /.row -->
		<div class="row">
			<div class="col-sm-12">
				<form method="post" action="<?=base_url()?>admin/add_new_district/<?=$this->uri->segment(3)?>/<?=$this->uri->segment(4)?>" enctype="multipart/form-data" >
					<?php if($this->session->flashdata('count')):?>
						<?php for($i=1;$i<=$this->session->flashdata('count');$i++):?>
							<?php if($i==1):?>
								<div class="row" id="row1">
									<div class="col-sm-3">					
										<label class="control-label" for="district1">Districts <a class="add-more" >+ Add new district</a></label>		
										<input value="<?=$this->session->flashdata('district1')?>" autocomplete="off" class="form-control" id="district1" name="district1" type="text" />							
									</div>	
									<div class="col-sm-9">					
										<label class="control-label" for="zipcodes1">Zipcodes (EX: 231range322,344,544,653)</label>									
										<input value="<?=$this->session->flashdata('zipcodes1')?>"  autocomplete="off" class="form-control" id="zipcodes1" name="zipcodes1" type="text" />
									</div>										
								</div>	
							<?php else:?>
								<br/>
								<div class="row" id="row<?=$i?>">
									<div class="col-sm-3">					
										<input value="<?=$this->session->flashdata('district'.$i)?>" autocomplete="off" class="form-control" id="district<?=$i?>" name="district<?=$i?>" type="text" />
									</div>	
									<div class="col-sm-7">						
										<input value="<?=$this->session->flashdata('zipcodes'.$i)?>"  autocomplete="off" class="form-control" id="zipcodes<?=$i?>" name="zipcodes<?=$i?>" type="text" />
									</div>	
									<div class="col-sm-2">
									<label class="control-label"><a class="remove-row">Remove</a></label>
									</div>
								</div>							 
							<?php endif;?> 
						<?php endfor;?>
					<?php else:?>
						<div class="row" id="row1">
							<div class="col-sm-3">					
								<label class="control-label" for="district1">Districts <a class="add-more" >+ Add new district</a></label>		
								<input autocomplete="off" class="form-control" id="district1" name="district1" type="text" />							
							</div>	
							<div class="col-sm-9">					
								<label class="control-label" for="zipcodes1">ZIP Codes (EX: 231range322,344,544,653)</label>									
								<input autocomplete="off" class="form-control" id="zipcodes1" name="zipcodes1" type="text" />									
							</div>										
						</div>	 
					<?php endif;?>			 		
					<br/>	
					<div class="row">
						<div class="col-sm-12">
							<input type="hidden" name="count" value="<?=($this->session->flashdata('count'))?$this->session->flashdata('count'):'1'?>" id="count" />
							<input class="btn btn-primary" type="submit" name="add_new_districts" value="Add district">
						</div>
					</div>				
				</form>
			</div>  
		</div>  
		<br/>
		<form method="post" action="">
		<?php foreach($districts as $district):?>
			<?php $value = (set_value($district->name.$district->id))?set_value($district->name.$district->id):$district->name;?>
			<div class="row">
				<div class="col-sm-3">					
					<input value="<?=$value?>" autocomplete="off" class="form-control" id="district<?=$district->id?>" name="district<?=$district->id?>" type="text" />
				</div>	
				<div class="col-sm-9">						
					<?php $otput=array(); foreach($district->zipcodes as $zip):?>
						<?php $otput[]= '<a href="'.base_url().'admin/edit_zip/'.$zip->id.'/'.$this->uri->segment(3).'">'.$zip->zip.'</a>'?>
					<?php endforeach;?>
					<?=implode(', ',$otput)?> 
				</div>	
			</div>
			<br/>
		<?php endforeach;?>	
			<div class="row">
				<div class="col-sm-12">
					<input class="btn btn-primary" type="submit" name="send" value="Save">
				</div>
			</div>		
		</form>
	</div><!-- /#page-wrapper -->
<?=$footer?>