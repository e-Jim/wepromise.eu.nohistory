<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller {
	 
	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('edri_front');
	}
	 
	public function index()
	{	
		$this->session->keep_flashdata('vp_confirmation_response');
		
		$this->load->library('geoip');

		$gi = geoip_open('./assets/GeoIP.dat', GEOIP_MEMORY_CACHE);
		$country = geoip_country_code_by_addr($gi, $_SERVER['REMOTE_ADDR']);
		geoip_close($gi);
		$countries 					=  $this->edri_front->get_countries();
		$country_codes 				=  $this->edri_front->country_codes($countries);
	
		//var_dump($country_codes);
		
		if(in_array($country,$country_codes)){
			
			if($this->edri_front->check_country_active_language($country)){
				$language_code = $this->edri_front->get_language_code_by_country_code(strtolower($country));
				redirect(strtolower($language_code));
			
			}
			else redirect('en');
			
		}
		else redirect('en');
		 
		
		
		
	}
	function home($language){
		$language					= mysql_real_escape_string($language);
		$lang						= $this->edri_front->language_id_from_code($language);
		$data['language_id']		= $lang->id;
		$data['support_kit']		= $lang->support_kit;
		
		$countries 					= $this->edri_front->get_countries();
		$country_codes 				= $this->edri_front->country_codes($countries);
		$country_candidates 		= $this->edri_front->get_country_candidates($countries);
		$data['content'] 			= $this->edri_front->get_translations($data['language_id']);
		$data['recents_candidates'] = $this->edri_front->get_recent_candidates();
		$header_data['languages'] 	= $this->edri_front->get_active_languages();
		$header_data['title'] 		= $data['content']->meta_title;
		$header_data['description'] = $data['content']->meta_description;
		$header_data['keywords'] 	= $data['content']->meta_keywords;
		$header_data['pages']		= $this->edri_front->get_pages($data['language_id']);
		$data['header'] 			= $this->blocks->get_header($header_data);
		$data['form_promise']		= $this->blocks->get_form_promise($data['content'], false);
		$data['videos']				= $this->db->get('videos')->result();
		$data['read_charter_link']  = $this->db->get_where('page_translations',array('language_id'=>$lang->id,'page_id'=>'2'))->row()->slug;
		$data['sociale_footer']		= $this->blocks->get_sociale_footer();
		$data['footer'] 			= $this->blocks->get_footer(array('generale','home_scripts'=>array('country_candidates'=>$country_candidates,'candidates'=>$data['content']->candidates),'typeahead'));
		$this->load->view('home', $data); 
	
	}
	function zip($language,$zip_id){ 
		$language 					= mysql_real_escape_string($language);
		$zip_id						= mysql_real_escape_string($zip_id);
		$lang						= $this->edri_front->language_id_from_code($language);
		$data['language_id']		= $lang->id;
		$data['support_kit']		= $lang->support_kit;
		
		$header_data['title'] 		= "Candidates by zipcode";
		$header_data['description'] = '';
		$header_data['keywords'] 	= '';
		$header_data['pages']		= $this->edri_front->get_pages($data['language_id']);
		$header_data['languages'] 	= $this->edri_front->get_active_languages();
		$data['header'] 			= $this->blocks->get_header($header_data);
		
		
		$data['content'] 			= $this->edri_front->get_translations($data['language_id']);
		$data['form_promise']		= $this->blocks->get_form_promise($data['content'], true);
		$data['candidates']			= $this->edri_front->get_candidates_by_zipcode($zip_id);	
		$data['search_term']		= $this->edri_front->get_zip_search_term($zip_id);
		
		$data['sociale_footer']		= $this->blocks->get_sociale_footer();
		$data['footer'] 			= $this->blocks->get_footer(array('generale','typeahead'));
		
		$this->load->view('list_results',$data);
	}
	function city($language,$city_id){
		$language 					= mysql_real_escape_string($language);
		$city_id					= mysql_real_escape_string($city_id);	
		
		$lang						= $this->edri_front->language_id_from_code($language);
		$data['language_id']		= $lang->id;
		$data['support_kit']		= $lang->support_kit;
		
		$header_data['title'] 		= "Candidates by city";
		$header_data['description'] = '';
		$header_data['keywords'] 	= '';		
		$header_data['pages']		= $this->edri_front->get_pages($data['language_id']);
		$header_data['languages'] 	= $this->edri_front->get_active_languages();
		$data['header'] 			= $this->blocks->get_header($header_data);	
		
		$data['content'] 			= $this->edri_front->get_translations($data['language_id']);
		$data['form_promise']		= $this->blocks->get_form_promise($data['content'], true);
		$data['candidates']			= $this->edri_front->get_candidates_by_city($city_id);	
		$data['search_term']		= $this->edri_front->get_city_search_term($city_id);
		
		$data['sociale_footer']		= $this->blocks->get_sociale_footer();
		$data['footer'] 			= $this->blocks->get_footer(array('generale','typeahead'));
		
		$this->load->view('list_results',$data);
	}
	function district($language,$district_id){
		$language 					= mysql_real_escape_string($language);
		$district_id				= mysql_real_escape_string($district_id);	
				
		$lang						= $this->edri_front->language_id_from_code($language);
		$data['language_id']		= $lang->id;
		$data['support_kit']		= $lang->support_kit;
		
		$header_data['title'] 		= "Candidates by zipcode";
		$header_data['description'] = '';
		$header_data['keywords'] 	= '';		
		$header_data['pages']		= $this->edri_front->get_pages($data['language_id']);
		$header_data['languages'] 	= $this->edri_front->get_active_languages();
		$data['header'] 			= $this->blocks->get_header($header_data);
			
		$data['content'] 			= $this->edri_front->get_translations($data['language_id']);
		$data['form_promise']		= $this->blocks->get_form_promise($data['content'], true);
		$data['candidates']			= $this->edri_front->get_candidates_by_district($district_id);		
		$data['search_term']		= $this->edri_front->get_district_search_term($district_id);
		
		$data['sociale_footer']		= $this->blocks->get_sociale_footer();
		$data['footer'] 			= $this->blocks->get_footer(array('generale','typeahead'));	
		
		$this->load->view('list_results',$data); 
		 
	}	
	function country($language,$country_id){
		$language 					= mysql_real_escape_string($language);
		$country_id				= mysql_real_escape_string($country_id);	
				
		$lang						= $this->edri_front->language_id_from_code($language);
		$data['language_id']		= $lang->id;
		$data['support_kit']		= $lang->support_kit;
		
		$header_data['title'] 		= "Candidates by zipcode";
		$header_data['description'] = '';
		$header_data['keywords'] 	= '';		
		$header_data['pages']		= $this->edri_front->get_pages($data['language_id']);
		$header_data['languages'] 	= $this->edri_front->get_active_languages();
		$data['header'] 			= $this->blocks->get_header($header_data);
			
		$data['content'] 			= $this->edri_front->get_translations($data['language_id']);
		$data['form_promise']		= $this->blocks->get_form_promise($data['content'], true);
		$data['candidates']			= $this->edri_front->get_candidates_by_country($country_id);		
		$data['search_term']		= $this->edri_front->get_country_search_term($country_id);
		
		$data['sociale_footer']		= $this->blocks->get_sociale_footer();
		$data['footer'] 			= $this->blocks->get_footer(array('generale','typeahead'));	
		
		$this->load->view('list_results',$data); 
		 
	}		
	function search_results($search){
		$search	= mysql_real_escape_string($search);
		$search = str_replace('%20',' ', $search);
	//	$search = html_entity_decode($search);
	//	var_dump($search);		
		$search = rawurldecode($search);
	//var_dump($search);
		$json=array(); 
		
		
		$this->db->select('countries.name as country_name,cities.name as city_name, cities.id as city_id');
		$this->db->join('countries','countries.id=cities.country_id');
		$this->db->like('cities.name',$search,'after');
		$this->db->limit(15); 
		$query = $this->db->get('cities');
		
	
		foreach ($query->result() as $row){
			$ar_ret['city_id'] 	  =  $row->city_id;
			$ar_ret['tokens'] = $row->city_name;
			$ar_ret['value']  = $row->city_name.', '.$row->country_name;
			$json[] = $ar_ret;
			unset($ar_ret);
		}		

		$this->db->select('countries.name as country_name,cities.name as city_name, cities.id as city_id,zipcodes.zip,zipcodes.id as zip_id');
		$this->db->join('cities','cities.id=zipcodes.city_id');
		$this->db->join('countries','countries.id=cities.country_id');
		$this->db->where('zipcodes.zip',$search);
		$this->db->limit(15);
		$query = $this->db->get('zipcodes');

		foreach ($query->result() as $row){
			$ar_ret['zip_id'] 	  	=  $row->zip_id;
			$ar_ret['tokens'] 		=  $row->zip;
			$ar_ret['value'] 		=  $row->zip.', '.$row->city_name.', '.$row->country_name;
			$json[]					=  $ar_ret;
			unset($ar_ret);
		}			

		$this->db->select('countries.name as country_name,cities.name as city_name, cities.id as city_id,districts.name as district_name ,districts.id as district_id');
		$this->db->join('cities','cities.id=districts.city_id');
		$this->db->join('countries','countries.id=cities.country_id');
		$this->db->like('districts.name',$search,'after');
		$this->db->limit(15);
		$query = $this->db->get('districts');

		foreach ($query->result() as $row){
			$ar_ret['district_id']  =  $row->district_id;
			$ar_ret['tokens'] 		=  $row->district_name;
			$ar_ret['value'] 		=  $row->district_name.', '.$row->city_name.', '.$row->country_name;
			$json[]					=  $ar_ret;
			unset($ar_ret);
		}		
		
		if(count($json)==0){ 
			
			$ar_ret['id'] 	  =  0;
			$ar_ret['tokens'] = "no_results";
			$ar_ret['value']  = "No results";
			$json[]			  = $ar_ret;
			
		}
	
		$data['json'] = $json;
		$this->load->view('results_json',$data);	
		
	}
	
	public function temp_search() {
		$this->load->view('temp_search');
	} 
	function page($language,$slug){
		$language					= mysql_real_escape_string($language);		
		$slug						= mysql_real_escape_string($slug);		
		$lang						= $this->edri_front->language_id_from_code($language);
		$data['language_id']		= $lang->id;
		$data['support_kit']		= $lang->support_kit;
		$data['content'] 			= $this->edri_front->get_translations($data['language_id']);
		$data['content_page']		= $this->edri_front->get_content_page($data['language_id'],$slug);
		$header_data['title'] 		= $data['content_page']->meta_title;
		$header_data['description'] = $data['content_page']->meta_description;
		$header_data['keywords'] 	= $data['content_page']->meta_keywords;	
		$header_data['pages']		= $this->edri_front->get_pages($data['language_id']);
		
		$header_data['languages'] 	= $this->edri_front->get_active_languages();
		$header_data['other_slugs'] = $this->edri_front->get_other_slugs($slug,$header_data['languages']);
		$data['header'] 			= $this->blocks->get_header($header_data);
		if($data['content_page']->page_id==6) 
		
		$data['partners'] = $this->db->get('partners')->result();
		else $data['partners'] = false;
		
		if($data['content_page']->page_id==2) 
		$data['tabs'] = true;
		else $data['tabs'] = false;
		
		$data['form_promise']		= $this->blocks->get_form_promise($data['content'], true);	
		
		$data['sociale_footer']		= $this->blocks->get_sociale_footer();
		$data['footer'] 			= $this->blocks->get_footer(array('generale','typeahead'));	
		$this->load->view('text_page',$data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */