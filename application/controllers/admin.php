<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('tank_auth');
		if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		} 
		
		$this->load->model('edri');
	
	}
	function index()
	{
		redirect('admin/candidates');
	}
	function countries()
	{
		$data_header['title']	= "All countries";
		$data['header'] 		= $this->blocks->get_header_admin($data_header);
		$data['footer'] 		= $this->blocks->get_footer_admin(array('table-sorter','modal_delete'));
		
		$data['countries'] = $this->edri->get_countries();
		$data['languages'] = $this->edri->fetch_array($this->edri->get_languages(),'name');
		
		if($this->input->post('send')) {
			$country_id = $this->input->post('country_id');
		
			if( isset( $_FILES['csv']['name'] ) && $_FILES['csv']['name']!=""&&$this->edri->check_import($country_id)){
			
				//import csv
				
				$file_ext 					= end( explode( '.', $_FILES['csv']['name'] ) );
				$config['upload_path'] 		= './uploads/import';
				$config['allowed_types'] 	= 'csv';
				$config['max_size']			= '0';
				$poza_fara_extensie			= strtolower(url_title($_FILES['csv']['name'])); 
				$config['file_name']		= $country_id.'-'.date('Y-m-d-H-i-s').'-'.$poza_fara_extensie.'.'.$file_ext;
				$this->load->library('upload', $config,'upload_csv');
				
				$file_state = $this->upload_csv->do_upload('csv');

				if($file_state) 
				{	 
					$data['csv']=$config['file_name'];
					
					$this->load->library('csvreader');
					$result  =   $this->csvreader->parse_file('./uploads/import/'.$data['csv']);
					//echo "<pre>";
					//print_r($result);
					//echo "</pre>";
					$this->edri->insert_csv($result,$country_id);
					$data_update['imported']	= "yes";
					
					$data_update['language_id'] = $this->input->post('language_id');
					$this->edri->update_country($data_update,$country_id);
					$this->session->set_flashdata('success', 'Country updated!');		
					redirect('admin/countries');
				}
				else {
					$this->session->set_flashdata('errors', $this->upload_csv->display_errors());
					redirect('admin/countries');
				}
 
			}
			else {
			
				$data_update['language_id'] = $this->input->post('language_id');
				$this->edri->update_country($data_update,$country_id);
					
				$this->session->set_flashdata('success', 'Country updated!');
				redirect('admin/countries');
			}	
		}
		else{ 

		$data['countries'] = $this->edri->get_countries_and_cities();
		$data['languages'] = $this->edri->fetch_array($this->edri->get_languages(),'name');
		$this->load->view('admin/countries',$data);
		}
	} 
	function delete_cities_from_country_id($country_id){
		
		$this->edri->clear_imported_data($country_id);
		$this->session->set_flashdata('success', 'Sucess! Data deleted.');
		redirect('admin/countries');
	
	}
	function add_city($country_id){
		$data_header['title']	= "Add new city";
		$data['header'] 		= $this->blocks->get_header_admin($data_header);
		$data['footer'] 		= $this->blocks->get_footer_admin(array('tooltip','add_city'));
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('city_name', 'City', 'callback_duplicate_city['.$country_id.']|required');		
		$this->form_validation->set_rules('count', 'Count', '');	
		for($i=1;$i<=$this->input->post('count');$i++){
			$this->form_validation->set_rules('district'.$i, 'District', 'required');
			$this->form_validation->set_rules('zipcodes'.$i, 'Zipcodes', ''); 
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/add_city', $data);
		}
		else
		{ 
			$update_data=$this->input->post(); 
			$this->edri->insert_city($country_id,$update_data);		
			redirect('admin/countries');
		}			
		 
	}
	function edit_city($city_id,$country_id){
		$data_header['title']	= "Edit city";
		$data['header'] 		= $this->blocks->get_header_admin($data_header);
		$data['footer'] 		= $this->blocks->get_footer_admin(array('tooltip','add_city'));
		$data['city']			= $this->edri->get_city($city_id);
		$data['districts']		= $this->edri->get_city_districts($city_id);
		$this->load->library('form_validation');
		
		
 
		foreach($data['districts'] as $district ){
			$this->form_validation->set_rules('district'.$district->id, 'District', 'callback_duplicate_district['.$district->id.']|required');
		}
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/edit_city', $data);
		}
		else
		{ 
			$update_data=$this->input->post(); 
		
			$this->edri->update_districts_name($country_id,$update_data);	
			$this->session->set_flashdata('success', 'Districts updated!');			
			redirect('admin/edit_city/'.$city_id.'/'.$country_id);
		}		
		
		
	}
	function add_new_district($city_id,$country_id){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('count', 'Count', '');	
		for($i=1;$i<=$this->input->post('count');$i++){
			$this->form_validation->set_rules('district'.$i, 'District', 'required');
			$this->form_validation->set_rules('zipcodes'.$i, 'ZIP Codes', 'required');
		}
		
		if ($this->form_validation->run() == FALSE)
		{	
			for($i=1;$i<=$this->input->post('count');$i++){
				$this->session->set_flashdata('district'.$i, $this->form_validation->set_value('district'.$i));
				$this->session->set_flashdata('zipcodes'.$i, $this->form_validation->set_value('zipcodes'.$i));
			} 
			$this->session->set_flashdata('count',$this->input->post('count'));
			 
			$this->session->set_flashdata('errors', validation_errors());
			redirect('admin/edit_city/'.$city_id.'/'.$country_id);
		} 
		else
		{ 
			$update_data=$this->input->post(); 
			$this->edri->insert_new_districts($city_id,$update_data);	
			$this->session->set_flashdata('success', "Districts added");			
			redirect('admin/edit_city/'.$city_id.'/'.$country_id);
		}			 
 
	}
	
	function edit_zip($zip_id,$city_id){
		$data_header['title']	= "Edit zip";
		$data['city']			= $this->edri->get_city($city_id);
		$data['zip']			= $this->edri->get_zip($zip_id);
		$data['header'] 		= $this->blocks->get_header_admin($data_header);
		$data['footer'] 		= $this->blocks->get_footer_admin(array());
		$this->load->library('form_validation');
		
		//$this->form_validation->set_rules('zipcode', 'ZIP code', 'callback_duplicate_zip['.$zip_id.']|required');
		$this->form_validation->set_rules('zipcode', 'ZIP code', 'required');

		 
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/edit_zip', $data);
		} 
		else
		{ 
			$zip_value = $this->form_validation->set_value('zipcode'); 
			$this->edri->update_zip($zip_id,$zip_value);	
			$this->session->set_flashdata('success', 'ZIP code updated!');			
			redirect('admin/edit_zip/'.$zip_id.'/'.$city_id);
		}		
		
			
	
	}
	function delete_zip($zip_id,$city_id,$country_id){
		$this->edri->delete_zipcode($zip_id);
		$this->session->set_flashdata('success', 'ZIP code deleted!');			
		redirect('admin/edit_city/'.$city_id.'/'.$country_id);
	}
	function languages(){

		$data_header['title']	= "All languages";
		$data['header'] 		= $this->blocks->get_header_admin($data_header);
		$data['footer'] 		= $this->blocks->get_footer_admin(array('boot-switch','table-sorter'));
		$data['languages'] 		= $this->edri->get_languages();	
		
		if($this->input->post('send')) {
			$update_data['status']		= $this->input->post('status');
			$check_translates = $this->edri->check_translates($this->input->post('language_id'));
			if(!$check_translates){
				$this->edri->update_language($update_data,$this->input->post('language_id'));
				$this->session->set_flashdata('success', 'Language updated!');
			} 
			else 
				$this->session->set_flashdata('errors',$this->edri->check_translates($this->input->post('language_id')));
				
			redirect('admin/languages');
		} 
			
		$this->load->view('admin/languages',$data);
		
	}
	
	function support_kits(){
	
		$data_header['title']	= "All Support kits";
		$data['header'] 		= $this->blocks->get_header_admin($data_header);
		$data['footer'] 		= $this->blocks->get_footer_admin(array('table-sorter'));
		$data['languages'] 		= $this->edri->get_languages();	
		
		if($this->input->post('send')) {

				 
			if( isset( $_FILES['support_kit']['name'] ) && $_FILES['support_kit']['name']!="" ){
				$file_ext 					= end( explode( '.', $_FILES['support_kit']['name'] ) );
				$config['upload_path'] 		= './uploads/support_kits';
				$config['allowed_types'] 	= '*';
				$config['max_size']			= '0';
				$config['file_name']		= $this->input->post('language_id').'-'.date('Y-m-d-H-i-s').'-support_kit.'.$file_ext;
				$this->load->library('upload', $config, 'upload_csv');
				
				$file_state = $this->upload_csv->do_upload('support_kit');

				if($file_state) 
				{	 
					$update_data['support_kit']=$config['file_name'];
					$this->edri->update_language($update_data,$this->input->post('language_id'));
					$this->session->set_flashdata('success', 'Support kit updated!');		
					//redirect('admin/support_kits');	
				}
				else $this->session->set_flashdata('errors', $this->upload_csv->display_errors());
 
			}	
		}
		
		$this->load->view('admin/upload_kits',$data);
		

	
	}

	function candidates(){
		$data_header['title']	= "All candidates";
		$data['header'] 		= $this->blocks->get_header_admin($data_header);
		$data['footer'] 		= $this->blocks->get_footer_admin(array('table-sorter-pager'));
		$data['candidates']		= $this->edri->get_candidates();

		$this->load->view('admin/candidates',$data);
	}
	
	function add_candidate(){
		$data_header['title']	 = "Add candidate";
		$data['header'] 		 = $this->blocks->get_header_admin($data_header);
		/*
		$data['footer'] 		 = $this->blocks->get_footer_admin(array('tooltip','wysihtml5-editor'=>array('candidate'=>true),'typeahead'=>array(
																	'countries_array' => $this->edri->fetch_array($this->edri->get_countries(),'name'),
																	'cities_array' 	  => $this->edri->fetch_array($this->edri->get_cities(),'name')
																	)));	
		*/
		$data['footer'] 		 = $this->blocks->get_footer_admin(array('tooltip','wysihtml5-editor'=>array('candidate'=>true),'typeahead'=>array('countries_array' => $this->edri->fetch_array($this->edri->get_countries(),'name'))));
		$data['languages']		 = $this->edri->get_active_languages();	
		
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name', 'callback_check_duplicate|required');
		$this->form_validation->set_rules('country_id', 'Country', 'required');
		//$this->form_validation->set_rules('city_id', 'City', 'required');
		
		$this->form_validation->set_rules('city_id', 'City', 'xss_clean');
		$this->form_validation->set_rules('website', 'Website', '');
		$this->form_validation->set_rules('party_name', 'Party name', '');
		$this->form_validation->set_rules('facebook', 'Facebook', '');
		$this->form_validation->set_rules('twitter', 'Twitter', '');
		$this->form_validation->set_rules('gplus', 'Google+', '');
		$this->form_validation->set_rules('youtube', 'Youtube', '');
		for($i=0;$i<24;$i++){ 
			$this->form_validation->set_rules('desc_'.$i, 'Description', '');
		}
		//$this->form_validation->set_rules('district_id', 'District', 'callback_district_selected|required');
		$this->form_validation->set_rules('district_id', 'District', 'xss_clean');
		
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/add_candidate',$data);	
		}
		else
		{
			$update_data=$this->input->post(); 
			$this->edri->insert_candidate($update_data);	
			
			redirect('admin/candidates');
		}	
	}	
	
	function candidate($id){
		$data_header['title']	= "Edit candidate";
		$data['header'] 		= $this->blocks->get_header_admin($data_header);
		$data['footer'] 		 = $this->blocks->get_footer_admin(array('tooltip','wysihtml5-editor'=>array('candidate'=>true),'typeahead_edit'=>array(
																	'countries_array' => $this->edri->fetch_array($this->edri->get_countries(),'name'),
				 													//'cities_array' 	  => $this->edri->fetch_array($this->edri->get_cities(),'name'),
				 													//'districts_array' => $this->edri->fetch_array($this->edri->get_districts(),'name'),
																	'candidate' 	  => $this->edri->get_candidate($id))));
						 													
		$data['languages']		= $this->edri->get_active_languages();
		$data['success']		= FALSE;
		 

		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name', 'callback_check_duplicate_update|required');
		$this->form_validation->set_rules('country_id', 'Country', 'required');
		//$this->form_validation->set_rules('city_id', 'City', 'required');
		$this->form_validation->set_rules('city_id', 'City', 'xss_clean');
		$this->form_validation->set_rules('website', 'Website', '');
		$this->form_validation->set_rules('party_name', 'Party name', '');
		$this->form_validation->set_rules('facebook', 'Facebook', '');
		$this->form_validation->set_rules('twitter', 'Twitter', '');
		$this->form_validation->set_rules('gplus', 'Google+', '');
		$this->form_validation->set_rules('youtube', 'Youtube', '');
		$this->form_validation->set_rules('winner', 'Winner', '');
		for($i=0;$i<24;$i++){
			$this->form_validation->set_rules('desc_'.$i, 'Description', '');
		}
		//$this->form_validation->set_rules('district_id', 'District', 'callback_district_selected|required');
		$this->form_validation->set_rules('district_id', 'District', 'xss_clean');
			
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/edit_candidate',$data);
		}
		else 
		{
			$update_data=$this->input->post(); 
			$this->edri->update_candidate($id,$update_data);	
			$this->session->set_flashdata('success', 'Candidate updated!');
			redirect('admin/candidate/'.$id);
			
		}		 
	} 
	function delete_candidate($candidate_id){
	
		$this->db->where('id',$candidate_id);
		$this->db->delete('candidates');
		$this->session->set_flashdata('success', 'Candidate deleted!');
		redirect('admin/candidates');
	
	}

	function pages($page_id){
		$data_header['title']	= "Edit page";
		$data['header'] 		= $this->blocks->get_header_admin($data_header);
		$data['footer'] 		 = $this->blocks->get_footer_admin(array('tooltip','wysihtml5-editor'));
		$data['languages']		= $this->edri->get_active_languages();
		
		if($this->input->post()){
			$content = $this->input->post(); 
			
			$this->edri->update_page($page_id,$content,count($data['languages']));
			$this->session->set_flashdata('success', 'Page updated!');
			redirect('admin/pages/'.$page_id);
		}
		$data['page'] = $this->edri->get_page($page_id);
		$this->load->view('admin/edit_page',$data);
	 
	}
	function translations(){
		$data_header['title']	= "Translations";
		$data['header'] 		= $this->blocks->get_header_admin($data_header);
		$data['footer'] 		 = $this->blocks->get_footer_admin(array('tooltip','wysihtml5-editor'));
		$data['languages']		= $this->edri->get_active_languages();
		
		if($this->input->post()){
			$content = $this->input->post(); 
			
			$this->edri->update_translations($content,count($data['languages']));
			$this->session->set_flashdata('success', 'Translations updated!');
			redirect('admin/translations');
		}
		$data['translations'] = $this->edri->get_translations();
		$this->load->view('admin/translations',$data);
	}
	function get_countries($search){
		$search = str_replace('%20',' ', $search);
		$search = str_replace('%C3%B1','ñ', $search);
		///$search = htmlentities($search);
		//$search = mysql_real_escape_string($search);
		$json=array(); 
		$this->db->like("name",$search,"after");
		$this->db->limit(10); 
		$query = $this->db->get("countries");

		foreach ($query->result() as $row){
			$ar_ret['id'] 	  =  $row->id;
			$ar_ret['tokens'] = $row->name;
			$ar_ret['value']  = $row->name;
			$json[] = $ar_ret;
			unset($ar_ret);
		}		
		
		$data['json'] = $json;
		$this->load->view('results_json',$data);	
	}
	
	function get_cities($country_id,$search){
		$search = str_replace('%20',' ', $search);
		$search = str_replace('%C3%B1','ñ', $search);
		//$search = str_replace('&#40;','(', $search);
		//$search = str_replace('&#41;','(', $search);
		//$search = mysql_real_escape_string($search);
		//$search = html_entity_decode($search);
	$search = rawurldecode($search);
		$json=array(); 
		$this->db->where("country_id",$country_id);
		$this->db->like("name",$search,"after",FALSE);
		$this->db->limit(10);  
		$query = $this->db->get("cities");
//	var_dump($this->db->last_query());
		foreach ($query->result() as $row){
			$ar_ret['id'] 	  =  $row->id;
			$ar_ret['tokens'] = $row->name;
			$ar_ret['value']  = $row->name;
			$json[] = $ar_ret;
			unset($ar_ret);
		}		
		
		$data['json'] = $json;
		$this->load->view('results_json',$data);	
	}
	function get_districts($city_id,$district_id=false){
	
		$returnez=""; 
		//$this->db->select('districts.id as district_id, districts.name');
		//$this->db->join('citi_district','citi_district.district_id=districts.id');
		//$this->db->where('citi_district.city_id',$city_id);		
		$this->db->where('city_id',$city_id);		
		$query = $this->db->get('districts');
		foreach ($query->result() as $row){
			if($district_id&&($district_id==$row->id))
				$returnez.="<option selected=selected value='".$row->id."'>".$row->name."</option>";
			else
				$returnez.="<option value='".$row->id."'>".$row->name."</option>";
		} 		
		echo $returnez;
		
	}		

	function partners()	{
	
		$data_header['title']	= "Edit partners";
		$data['header'] 		= $this->blocks->get_header_admin($data_header);
		$data['footer'] 		 = $this->blocks->get_footer_admin(array());
		if($this->input->post('send')) {

			if( isset( $_FILES['partners']['name'] )&&  $_FILES['partners']['name']!=""){
						
				//update poza


				$file_ext 						= end( explode( '.', $_FILES['partners']['name'] ) );
				$config['upload_path'] 			=	 './uploads/partners/temp';
				$config['allowed_types'] 		= '*';
				$config['max_size']				= '0';
				$poza_fara_extensie				= strtolower(url_title($this->input->post('name')));
				$config['file_name']			= $poza_fara_extensie.'_'.time().'.'.$file_ext;
				
				$this->load->library('upload', $config);		
				$file_state 					= $this->upload->do_upload('partners');
	  
				if($file_state)
				{	

					$data['image']=$config['file_name'];
					$from = $config['upload_path']."/".$config['file_name'];
					$file_ext 					 = end( explode( '.', $data['image']) );

					$this->load->library('image_lib');
					 
					$configBig 					 = array();
					$configBig['image_library']  = 'gd2';
					$configBig['source_image']   = $from;
					$configBig['new_image']		 = './uploads/partners';
					$configBig['maintain_ratio'] = TRUE;
					$configBig['create_thumb']   = FALSE;
					$configBig['quality']		 = '100%';
					$configBig['width'] 		 = 550;
					$configBig['height'] 		 = 330;
					$this->image_lib->initialize($configBig); 
					$this->image_lib->resize();
					$this->image_lib->clear();
					unset($configBig);

					unlink($from);
					$insert_data['page_id'] = 6;
					$insert_data['image'] 	= $config['file_name'];
					$insert_data['link'] 	= $this->input->post('link');
					$insert_data['name'] 	= $this->input->post('name');
					$this->db->insert('partners',$insert_data);
					$this->session->set_flashdata('success', 'Partner added!');
					redirect('admin/partners');
				}
			}
		}
		if($this->input->post('save_partner')){
			$this->db->where('id',$this->input->post('partner_id'));
			$this->db->update('partners',array('name'=>$this->input->post('name'),'link'=>$this->input->post('link')));
			$this->session->set_flashdata('success', 'Partner updated!');
			redirect('admin/partners');			
		}

	
		$data['partners'] = $this->db->get('partners')->result();		
		$this->load->view('admin/partners',$data); 

	}
	function delete_partner($id){
	
		$this->db->where('id',$id);
		$this->db->delete('partners');
		$this->session->set_flashdata('success', 'Partner deleted!');
		redirect('admin/partners');			

	}
	
	function videos(){
		$data_header['title']	= "Edit videos";
		$data['header'] 		= $this->blocks->get_header_admin($data_header);
		$data['footer'] 		 = $this->blocks->get_footer_admin(array());
		if($this->input->post('send')) {
		
			$insert_data['link'] 	= $this->input->post('link');
			$insert_data['name'] 	= $this->input->post('name');
			$this->db->insert('videos',$insert_data);
			$this->session->set_flashdata('success', 'Video added!');
			redirect('admin/videos');

		}
		if($this->input->post('save_video')){
			$this->db->where('id',$this->input->post('video_id'));
			$this->db->update('videos',array('name'=>$this->input->post('name'),'link'=>$this->input->post('link')));
			$this->session->set_flashdata('success', 'Video updated!');
			redirect('admin/videos');			
		}

	
		$data['videos'] = $this->db->get('videos')->result();		
		$this->load->view('admin/videos',$data); 
	}
	function delete_video($id){
	
		$this->db->where('id',$id);
		$this->db->delete('videos');
		$this->session->set_flashdata('success', 'Video deleted!');
		redirect('admin/videos');			

	}	
 // extra validation functions	
	//check duplicate name for candidates.
	public function check_duplicate($str)
	{
		if(!$this->edri->check_duplicate_name($str)){
			$this->form_validation->set_message('check_duplicate', 'This %s is already used!');
			return FALSE;
		}
		else
			return TRUE;
		
	}	
	public function check_duplicate_update($str)
	{
		if(!$this->edri->check_duplicate_name_on_update($str)){
			$this->form_validation->set_message('check_duplicate_update', 'This %s is already used!');
			return FALSE;
		}
		else
			return TRUE;
		
	}
	
	public function district_selected($str){
	
		if($str=='Please select district'){
			$this->form_validation->set_message('district_selected', 'The %s field is required.');
			return FALSE;
		}
		else
			return TRUE;	
	
	}
	
	public function duplicate_city($str,$country_id){
	
		if(!$this->edri->check_duplicate_city($str,$country_id)){
			$this->form_validation->set_message('duplicate_city', 'This %s is already used!');
			return FALSE;
		}
		else
			return TRUE;	
	
	}
	
	public function duplicate_district($str,$district_id){
	
		if($this->edri->check_duplicate_district($str,$district_id)){
			$current_name = $this->edri->get_district($district_id);
			$this->form_validation->set_message('duplicate_district', 'You can\'t change from '.$current_name->name.' to '.$str .'. '.$str .' is already used for a district in this city');
			return FALSE;
		}
		else
			return TRUE;	
	
	}
	
	public function duplicate_zip($str,$zip_id){
	
		if($this->edri->check_duplicate_zip($str,$zip_id)){
			
			$this->form_validation->set_message('duplicate_zip', 'This %s is already used!');
			return FALSE;
		}
		else
			return TRUE;	
	
	}
	
function get_subscribers(){

$q = $this->db->query("SELECT * FROM vote_promises");
		$data=array();
       if($q->num_rows() > 0):
            foreach($q->result() as $row2):
                $data[$row2->id]['id'] = $row2->id;
				$data[$row2->id]['name']=$row2->name;
				$data[$row2->id]['email']=$row2->email;
				$data[$row2->id]['country_code']=$row2->country_code;
				
				$data[$row2->id]['validated'] = $row2->validated;
				$data[$row2->id]['receive_news'] = $row2->receive_news;

            endforeach;
       endif;
	  
			// file name for download
			$filename = "subscibers" . date('Ymd') . ".xls";

			header("Content-Disposition: attachment; filename=\"$filename\"");
			header("Content-Type: application/vnd.ms-excel");

			$flag = false;
			foreach($data as $row):

				if(!$flag) {
				// display field/column names as first row
					echo implode("\t", array_keys($row)) . "\r\n";
					$flag = true;
				}
				//array_walk($row, 'cleanData');
				echo implode("\t", array_values($row)) . "\r\n";
			endforeach;	
			exit;

}
	// 
	/*
	function get_districts($city_id,$search){
	$search = str_replace('%20',' ', $search);
		$json=array(); 
		$this->db->select('districts.id as district_id, districts.name');
		$this->db->join('citi_district','citi_district.city_id=districts.id');
		if($city_id){
			$this->db->where('citi_district.city_id',$city_id);
			$this->db->like('districts.name',$search,'after');
		
			$this->db->limit(10); 
		}
		$query = $this->db->get('districts');

		foreach ($query->result() as $row){
			$ar_ret['id'] 	  =  $row->district_id;
			$ar_ret['tokens'] = $row->name;
			$ar_ret['value']  = $row->name;
			$json[] = $ar_ret;
			unset($ar_ret);
		}		
		 
		$data['json'] = $json;
		$this->load->view('results',$data);	
	}
	*/ 
}