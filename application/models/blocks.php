<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Blocks extends CI_Model
{
	function __construct()
	{
		parent::__construct();

	}

	function get_header_admin($data){
	
		return $this->load->view('admin/blocks/header',$data,TRUE);
	
	}
	function get_footer_admin($views){
		$returnam = "";
		$data = array();
		foreach($views as $view=>$value) {
			$data_scripts_js = array();
			
			if(is_array($value)) 
					foreach($value as $row=>$val) {	
						$data_scripts_js[$row]=$val;
					}
			if(!is_array($value)) 	
				$returnam .= $this->load->view('admin/footer_scripts/'.$value,'',TRUE);
			else 
				$returnam .= $this->load->view('admin/footer_scripts/'.$view,$data_scripts_js,TRUE);	
				
		}
		
		$data['footer_scripts'] = $returnam;
		return $this->load->view('admin/blocks/footer',$data,TRUE);
	}

	function get_header($data){
	
		return $this->load->view('blocks/header',$data,TRUE);
	
	}
	function get_footer($views){
		$returnam = "";
		$data = array();
		foreach($views as $view=>$value) {
			$data_scripts_js = array();
			
			if(is_array($value)) 
					foreach($value as $row=>$val) {	
						$data_scripts_js[$row]=$val;
					}
		if(!is_array($value)) 	
			$returnam .= $this->load->view('footer_scripts/'.$value,'',TRUE);
		else 
			$returnam .= $this->load->view('footer_scripts/'.$view,$data_scripts_js,TRUE);	
			
		}
		
		$data['footer_scripts'] = $returnam;
		return $this->load->view('blocks/footer',$data,TRUE);
	}
	
	function get_form_promise($content, $reversed){
		
		$this->load->library('session');
	
		$data['promises'] = $this->edri_front->get_nr_promises();
		$data['content']  = $content;
		$data['reversed'] = $reversed;
		return $this->load->view('blocks/form_promise',$data,TRUE);
		
	
	}	
	function get_sociale_footer(){

		return $this->load->view('blocks/sociale_footer','',TRUE);
		
	
	}
}